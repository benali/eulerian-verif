import stainless.lang._
import stainless.collection._
import stainless.annotation._

object EulerianPathsVerif {
  type Row = List[BigInt]
  type AdjacencyMatrix = List[Row] // List[OutEdges]

  // op redefine
  def forall[A](l: List[A], cond: A => Boolean): Boolean = l match {
    case Cons(head, tail) => cond(head) && forall(tail, cond)
    case _                => true
  }

  // op redefine
  def zipWithIndex[A](l: List[A]): List[(A, BigInt)] = {
    val ids = getIndex(l)
    require(isIndex(ids))
    l.zip(ids)
  }

  // op redefine
  def takeRight[A](l: List[A], n: BigInt): List[A] = l.reverse.take(n).reverse

  // matrix op redefine
  def transpose(m: AdjacencyMatrix): AdjacencyMatrix = {
    require(isMatrix(m))
    if (m.length == 0) m
    else {
      def help(a: AdjacencyMatrix, i: BigInt): AdjacencyMatrix = {
        require(isMatrix(a))
        require(i >= 0 && i <= m.length)
        require(a.length == m(0).length)
        decreases(i)
        if (i == 0) a
        else {
          val res: AdjacencyMatrix = zipWithIndex(a) map { case (row, j) =>
            Cons(m(i - 1)(j), row)
          }
          // assert(isMatrix(res))
          help(res, i - 1)
        }
      }
      val a: AdjacencyMatrix = (for (col <- m(0)) yield List[BigInt]())
      val res = help(a, m.length)
      // assert(getIndex(m) forall { case i =>
      //   getIndex(m(0)) forall { case j => m(i)(j) == res(j)(i) }
      // })
      res
    }
  }

  // op redefine
  def shape(m: AdjacencyMatrix): (BigInt, BigInt) = {
    require(isMatrix(m))
    val n = m.length
    (n, if (n > 0) m(0).length else 0)
  }

  // op redefine
  def diag(m: AdjacencyMatrix): List[BigInt] = {
    require(isSquareMatrix(m))
    getIndex(m) map { case i => m(i)(i) }
  }

  // op redefine
  def sum(l: List[BigInt]): BigInt = l match {
    case Cons(head, tail) => head + sum(tail)
    case Nil()            => BigInt(0)
  }

  // op redefine
  def distinct[A](l: List[A]): List[A] = {
    def subDistinct(l_ : List[A], res: List[A]): List[A] = l_ match {
      case Cons(head, tail) =>
        if (res.contains(head)) subDistinct(tail, res)
        else subDistinct(tail, res :+ head)
      case _ => res
    }
    subDistinct(l, List[A]())
  }

  // helper
  def shift[A](l: List[A]): List[(A, A)] = l match {
    case Cons(_, Cons(_, _)) => l.zip(l.tail)
    case _                   => Nil()
  }

  // helper
  def getIndex[A](l: List[A]): List[BigInt] = {
    List.range(0, l.length)
  } ensuring { isIndex(getIndex(l)) }

  // helper
  // @extern
  // def ident(n: BigInt): AdjacencyMatrix = (for (i <- List.range(0, n))
  //   yield List.fill(i)(0) ++ List(1) ++ List.fill(n - i - 1)(0)) map {
  //   BigInt
  // }

  // helper
  def bool2int(b: Boolean): BigInt = if (b) 1 else 0

  // helper
  def emptyGraph(n: BigInt): AdjacencyMatrix = {
    require(n >= 0)
    def emptyRow(n: BigInt): Row = List.range(0, n) map { case _ => 0 }

    List.range(0, n) map { case _ => emptyRow(n) }
  }

  // helper op
  def concat(m1: AdjacencyMatrix, m2: AdjacencyMatrix): AdjacencyMatrix = {
    require(isMatrix(m1) && isMatrix(m2))
    val (n1, n2) = (shape(m1)._1, shape(m2)._1)
    require(n1 == n2 || n1 == 0 || n2 == 0)
    if (n1 == 0) m2
    else if (n2 == 0) m1
    else m1.zip(m2) map { case (l, r) => l ++ r }
  }

  // helper op
  def expendAt[A](l: List[A], pos: BigInt, nl: List[A]): List[A] =
    l.take(pos) ++ nl ++ takeRight(l, l.length - pos - 1)

  // helper op
  def replace[A](l: List[A], pos: BigInt, newVal: A): List[A] =
    expendAt(l, pos, List(newVal))

  // helper op
  def totalDegree(m: AdjacencyMatrix): BigInt = {
    require(isMatrix(m) && posValues(m))
    sum(outDegree(m))
  }

  // helper op
  def plus(m1: AdjacencyMatrix, m2: AdjacencyMatrix): AdjacencyMatrix = {
    require(isMatrix(m1) && isMatrix(m2) && shape(m1) == shape(m2))
    def addRow(l1: Row, l2: Row): Row =
      l1.zip(l2).map { case (l, r) => l + r }
    m1.zip(m2) map { case (l, r) => addRow(l, r) }
  }

  // helper op
  def scalar(l1: Row, l2: Row): BigInt = {
    require(l1.length == l2.length)
    sum(l1.zip(l2).map { case (l, r) => l * r })
  }

  // helper op
  def mul(m1: AdjacencyMatrix, m2: AdjacencyMatrix): AdjacencyMatrix = {
    require(isMatrix(m1) && isMatrix(m2))
    require(shape(m1)._2 == shape(m2)._1)
    m1 map { case l => transpose(m2) map { case c => scalar(l, c) } }
  }

  // helper property
  def allEqual[B](l: List[(B, B)]): Boolean = l.forall { case (l, r) => l == r }

  // helper property
  def isIndex(ids: List[BigInt]) =
    ids.zip(shift(ids) map { case (l, r) => l - r }).forall { case (l, r) =>
      l < ids.length && r == -1
    }

  // helper property
  def isSquareMatrix(m: AdjacencyMatrix): Boolean = {
    val (n, p) = shape(m)
    isMatrix(m) && n == p
  }

  // helper property
  def isPositive(n: BigInt): Boolean = n >= 0
  // helper property
  def isStrictlyPositive(n: BigInt) = n > 0
  // helper property
  def posRow(r: Row): Boolean = forall(r, isPositive)
  // helper property
  def posValues(m: AdjacencyMatrix): Boolean = {
    require(isMatrix(m))
    forall(m, posRow)
  }

  // helper property
  def degreeCond(m: AdjacencyMatrix, isDirected: Boolean): List[BigInt] = {
    require(isAdjacencyMatrix(m, isDirected))
    if (isDirected) {
      val outD: List[BigInt] = outDegree(m)
      val inD: List[BigInt] = inDegree(m)
      outD
        .zip(inD)
        .map { case (o, i) => o - i }
    } else (outDegree(m) map { _ % 2 })
  }

  // property
  def isMatrix(m: AdjacencyMatrix): Boolean = {
    val lengths: List[BigInt] = m.map(_.length)
    allEqual(shift(lengths)) && forall(lengths, isStrictlyPositive)
  }

  // property
  def isAdjacencyMatrix(m: AdjacencyMatrix, isDirected: Boolean): Boolean = {
    isSquareMatrix(m) && (isDirected || m == transpose(m)) && diag(m).forall {
      _ == 0
    } && posValues(m) // && n < 10
  }

  // property
  // not sure this works for undirected graphs
  def isConnected(m: AdjacencyMatrix, isDirected: Boolean): Boolean = {
    require(isAdjacencyMatrix(m, isDirected))
    if (isDirected) true // assume because don't know how to check
    else {
      val (n, _) = shape(m)
      val l: List[AdjacencyMatrix] = List.fill(n - 2)(m)
      val kWalks = l.scanLeft(m)(mul(_, _))
      kWalks.foldLeft(emptyGraph(n))(plus) forall { _ forall { _ != 0 } }
    }
  }

  // property
  def hasEulerianCycle(m: AdjacencyMatrix, isDirected: Boolean): Boolean =
    isConnected(m, isDirected) && (degreeCond(m, isDirected) filter {
      _ != 0
    }) == Nil()

  // helper property
  def hasEdge(m: AdjacencyMatrix): Boolean = m exists { _ exists { _ != 0 } }

  // property
  // def hasEulerianPath(m: AdjacencyMatrix, isDirected: Boolean): Boolean =
  //   if (isDirected) {
  //     val d = degreeCond(m, isDirected) filter { _ != 0 }
  //     (d == List(-1, 1)) || (d == List(1, -1) || d == Nil)
  //   } else {
  //     val l = degreeCond(m, isDirected).filter { _ != 0 }.length
  //     (l == 0) || (l == 2)
  //   }

  // graph op
  def outDegree(m: AdjacencyMatrix): List[BigInt] = {
    require(isMatrix(m))
    m match {
      case Cons(head, tail) => Cons(sum(head), outDegree(tail))
      case Nil()            => Nil[BigInt]()
    }
  }

  // graph op
  def inDegree(m: AdjacencyMatrix): List[BigInt] = {
    require(isMatrix(m))
    outDegree(transpose(m))
  }

  // graph op
  def remove(
      m: AdjacencyMatrix,
      c1: BigInt,
      c2: BigInt,
      is_directed: Boolean
  ): AdjacencyMatrix = {
    val (n, p) = shape(m)
    require(0 <= c1 && c1 < n && 0 <= c2 && c2 < p && m(c1)(c2) > 0)
    def drop(l: Row, c: BigInt): Row = replace(l, c, l(c) - 1)
    def update(m: AdjacencyMatrix, c1: BigInt, c2: BigInt): AdjacencyMatrix =
      replace(m, c1, drop(m(c1), c2))
    val res = update(m, c1, c2)
    if (is_directed) res else update(res, c2, c1)
  }

  def walk(
      m: AdjacencyMatrix,
      isDirected: Boolean,
      path: List[BigInt]
  ): (AdjacencyMatrix, List[BigInt]) = {
    require(path.length > 0)
    require(isAdjacencyMatrix(m, isDirected))
    decreases(totalDegree(m))
    val start: BigInt = path.last
    val curr: BigInt = path.head
    val next: BigInt = m(curr).indexWhere(_ != 0)
    val newM: AdjacencyMatrix = remove(m, curr, next, isDirected)
    if (next == start) (newM, Cons(next, path))
    else walk(newM, isDirected, Cons(next, path))
  }

  def explore(
      m: AdjacencyMatrix,
      isDirected: Boolean,
      path: List[BigInt]
  ): List[BigInt] = {
    require(isAdjacencyMatrix(m, isDirected))
    require(path.length > 0)
    if (hasEdge(m)) {
      val start: BigInt =
        (distinct(path) filter { m(_) exists { _ != 0 } }).head
      val index: BigInt = path.indexOf(start)
      val (newM, newPath) =
        walk(m, isDirected, List(start))
      explore(newM, isDirected, expendAt(path, index, newPath))
    } else path.reverse
  }

  def hierholzer(m: AdjacencyMatrix, isDirected: Boolean): List[BigInt] = {
    require(
      isAdjacencyMatrix(m, isDirected) && hasEulerianCycle(m, isDirected)
    )
    explore(m, isDirected, List(m.indexWhere(_ exists { _ != 0 })))
  }

  // lemma
  def forallImpliesHead[A](l: List[A], cond: A => Boolean): Unit = {
    require(forall(l, cond) && l.length > 0)
  } ensuring { cond(l.head) }

  // lemma
  def forallImpliesTail[A](l: List[A], cond: A => Boolean): Unit = {
    require(forall(l, cond) && l.length > 0)
  } ensuring { forall(l.tail, cond) }

  // lemma
  def matrixTailIsMatrix(m: AdjacencyMatrix): Unit = {
    require(isMatrix(m) && m.length > 0)
    // val lengths: List[BigInt] = m.map(_.length)
    // val shifted: List[(BigInt, BigInt)] = shift(lengths)
    // forallImpliesTail(shifted, equalPair)
    // assert(allEqual(shift(lengths).tail))
    // assert(allEqual(shift(lengths.tail)))
    // forallImpliesTail(lengths, isStrictlyPositive)
    // assert(forall(lengths.tail, isStrictlyPositive))

  } ensuring { isMatrix(m.tail) }

  // lemma
  def sumPositive(a: BigInt, b: BigInt): Unit = {
    require(a >= 0 && b >= 0)
  } ensuring { a + b >= 0 }

  // lemma
  def sumNilZero(): Unit = {} ensuring { sum(Nil()) == BigInt(0) }

  // lemma
  def sumListPositive(l: List[BigInt]): Unit = {
    require(posRow(l))
    l match {
      case Cons(head, tail) => {
        sumListPositive(tail)
        sumPositive(head, sum(tail))
      }
      case _ => sumNilZero()
    }
  } ensuring { sum(l) >= 0 }

  // lemma
  def degreePositive(m: AdjacencyMatrix): Unit = {
    require(isMatrix(m) && posValues(m))
    // m match {
    //   case Cons(head, tail) => {
    //     assert(posRow(head))
    //     sumListPositive(head)
    //     matrixTailIsMatrix(m)
    //     assert(isMatrix(tail))
    //     assert(posValues(tail))
    //     degreePositive(tail)
    //   }
    //   case Nil() => {}
    // }
  } ensuring {
    forall(outDegree(m), isPositive)
  }

  // lemma
  def totalDegreeDecreasesAfterRemove(
      m: AdjacencyMatrix,
      c1: BigInt,
      c2: BigInt,
      isDirected: Boolean
  ) = {
    require(isAdjacencyMatrix(m, isDirected))
    val (n, p) = shape(m)
    require(0 <= c1 && c1 < n && 0 <= c2 && c2 < p && m(c1)(c2) > 0)
  } ensuring {
    totalDegree(remove(m, c1, c2, isDirected)) < totalDegree(m)
  }

  // lemma
  def totalDegreePositive(m: AdjacencyMatrix): Unit = {
    require(isMatrix(m) && posValues(m))
    degreePositive(m)
  } ensuring { totalDegree(m) >= 0 }
}
